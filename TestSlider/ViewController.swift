
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet private weak var sliderValueLabel: UILabel!
    @IBOutlet private weak var slider: UISlider!
    
    @IBAction func sliderTapped(_ sender: UITapGestureRecognizer) {
        handleSliderTap(sender)
    }
    @IBAction private func sliderValueChanged(_ sender: UISlider) {
        handleSliderChangedValue(sender)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSlider()
    }
}

// MARK: - Setting up Views

private extension ViewController {
    
    func setupSlider() {
        slider.setValue(0.5, animated: false)
        slider.sendActions(for: .valueChanged)
    }
}

// MARK: - Updating UI

private extension ViewController {
    
    func updateUI(progress: CGFloat) {
        sliderValueLabel.text = "\(progress)"
        view.backgroundColor = UIColor(displayP3Red: progress, green: progress, blue: progress, alpha: progress)
    }
}

// MARK: - Slider

private extension ViewController {

    // TODO: handle the case when the user taps on the bounds of the slider
    // (right now it won't translate those touches to min/max values)
    func handleSliderTap(_ sender: UITapGestureRecognizer) {
        let progress = sender.location(in: sender.view).x / slider.bounds.width
        let delta = slider.maximumValue - slider.minimumValue
        let newValue = slider.minimumValue + Float(progress) * delta
        slider.setValue(newValue, animated: true)
        slider.sendActions(for: .valueChanged)
    }
    
    func handleSliderChangedValue(_ sender: UISlider) {
        let progress = CGFloat(slider.value / (slider.maximumValue - slider.minimumValue))
        updateUI(progress: progress)
    }
}
